import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect : '/dashboard'
  },
  {
    path: '/dashboard',
    name : 'dashboard',
    component: ()=> import('../views/dashboard.vue')
  },
  {
    path: '/students',
    name : 'students',
    component: ()=> import('../views/students.vue')
  },
  {
    path: '/teachers',
    name : 'teachers',
    component: ()=> import('../views/teachers.vue')
  },
  {
    path: '/teachers/:major',
    name : 'teachers',
    component: ()=> import('../views/teachers.vue')
  },
  {
    path: '/lessons',
    name : 'lessons',
    component: ()=> import('../views/lessons.vue')
  },
  {
    path: '/chooseLesson',
    name : 'chooseLesson',
    component: ()=> import('../views/chooseLessons.vue')
  },
  {
    path : '/editStudent/:id',
    name : 'editStudent',
    component: ()=> import('../views/edit-student.vue')
  },
  {
    path: '*',
    name: "404page",
    component: ()=> import('../components/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})


router.beforeEach((to, from , next) => {
	//console.log(to);
	next(); 
})

export default router
