import Vue from 'vue'
import app from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue } from 'bootstrap-vue'
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import en from 'vee-validate/dist/locale/en.json';
import * as rules from 'vee-validate/dist/rules';
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueCookies from 'vue-cookies'
import  interceptorsHandler  from './axios/interceptors'
import interceptors from './axios/interceptors'
import '../assets/sass/style.scss'
import VueToast from 'vue-toast-notification';
// Import one of available themes
import 'vue-toast-notification/dist/theme-default.css';
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.use(Loading,{
	color: '#94f7b5',
	loader: 'bars',
	width: 64,
	height: 64,
	backgroundColor: '#ffffff',
	opacity: 0.5,
	zIndex: 999,
});
 
Vue.use(VueToast, {
	// One of options
	position: 'top'
})

Object.keys(rules).forEach(rule => {
	extend(rule, rules[rule]);
  });
  
localize('en', en);

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(VueCookies)
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.$cookies.set('theme','default');
Vue.$cookies.set('hover-time','1s');


Vue.config.productionTip = false;

axios.interceptors.request.use((request)=>{
	interceptorsHandler.request(request);
	return request;
} , (error)=>{
	interceptorsHandler.requestError(error);
	return Promise.reject(error)
});

axios.interceptors.response.use((response)=>{
	interceptorsHandler.response(response);	
	return response;
} , err=>{
	interceptorsHandler.responseError(err);
	return Promise.reject(err);
})

new Vue({
  router,
  store,
  render: h => h(app)
}).$mount('#app')

