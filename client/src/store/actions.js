import {services} from './services' 
import Axios from 'axios'
export const actions = {
    async fetchAllStudent({commit}){
        commit("fetchAllStudent" , await services.fetchAllStudent())
    },
    setStudentID ({commit} , params){
        commit("setStudentID" , params)
    },
    setStudentField ({commit} , params){
        commit("setStudentField" , params)
    },
    setLessonFeild ({commit} , params){
        commit("setLessonFeild" , params)
    },
    chooseLesson({commit} , params){
        commit("chooseLesson" , params)
    },
    setFinalLesson({commit} , params){
        commit("setFinalLesson" , params)
    },
    async fetchFinalLesson({commit} , params){
        await services.fetchFinalLesson(params).then((res)=>{
            commit("fetchFinalLesson" , res.data)
        }).catch((error)=>{
            commit("fetchFinalLesson" , [])
        })
            
    }
}