import axios from 'axios'
export const services = {
    fetchAllStudent(){
        return axios.get("/myapi/students")
    },
    fetchFinalLesson(id){
        return axios.get("/myapi/selectedlessons", {
            params:{
              id : id
            }
          })
    }
}
