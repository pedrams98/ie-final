export const mutations = {
    fetchAllStudent(state , params){
        state.allStudents = params.data
    },
    setStudentID(state , params){
        state.studentId = params;
    },
    setStudentField(state , params){
        state.studentFiled = params;
    },
    setLessonFeild(state , params){
        state.lessonFiled = params;
    },
    chooseLesson(state , params){
        state.choosenLesson.push(params)
    },
    setFinalLesson(state , params){
        state.choosenLesson.remove(params)
    },
    fetchFinalLesson(state , params){
        state.finalLessons = params;
    }
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};