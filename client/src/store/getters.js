export const getters = {
    isStudent : (state) => (id)=>{
        let bool = false;
        let students = state.allStudents
        students.forEach(student=>{
            if(student.id == id)
                bool = true;
        })
        return bool
    },
    getStudent : (state)=> (id)=>{
        let students = state.allStudents
        let student= {};
        students.forEach(s=>{
            if(s.id == id)
                student  = s 
        })
        return student
    },
    isLessthanSevenSelected : (state)=> (count)=>{
        let choosenLessons = state.choosenLesson
        choosenLessons.forEach(item=>{
            count = count+item.counter
        })
        if(count <= 6)
            return true
        else    
            return false    
    },
    isLessthanSeven : (state)=> (count)=>{
        let finalLesson = state.finalLessons;
        let choosenLessons = state.choosenLesson
        finalLesson.forEach(item=>{
            count = count+item.counter
        })
        choosenLessons.forEach(item=>{
            count = count+item.counter
        })
        if(count <= 6)
            return true
        else    
            return false 
    },
    isLessonSelected: (state)=> (id)=>{
        let bool = false;
        let choosenLessons = state.choosenLesson
        choosenLessons.forEach(item=>{
            if(item.lessonId == id)
                bool = true
        })
        return bool;
    },
    isFinalLesson: (state)=> (id)=>{
        let bool = false;
        let finalLesson = state.finalLessons;
        finalLesson.forEach(item=>{
            if(item.lessoncode == id)
                bool = true
        })
        return bool;
    },
    getChoosenLessons(state){
        return state.choosenLesson
    },
    isInEqualTerm: (state)=> (lesonTerm)=> {
        let finalLesson = state.finalLessons;
        let choosenLessons = state.choosenLesson;
        let bool = true;
        finalLesson.forEach(l =>{
            if(l.term !== lesonTerm)
                bool = false;
        })
        choosenLessons.forEach(l =>{            
            if(l.term != lesonTerm)
                bool = false;
        })
        console.log(bool);
        return bool;
    }
}