PGDMP             	            x           iefinal    10.6    10.1     }           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            ~           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       1262    24576    iefinal    DATABASE     �   CREATE DATABASE iefinal WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE iefinal;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12278    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24577    lessons    TABLE     �   CREATE TABLE lessons (
    name text,
    code bigint NOT NULL,
    counter integer,
    teachercode bigint,
    numberofstudents integer,
    term text,
    field text
);
    DROP TABLE public.lessons;
       public         postgres    false    3            �            1259    24606    selectedlessons    TABLE     N   CREATE TABLE selectedlessons (
    studentid bigint,
    lessoncode bigint
);
 #   DROP TABLE public.selectedlessons;
       public         postgres    false    3            �            1259    24580    students    TABLE     �   CREATE TABLE students (
    name text,
    lastname text,
    id bigint NOT NULL,
    field text,
    counter integer,
    mt text
);
    DROP TABLE public.students;
       public         postgres    false    3            �            1259    24588    teachers    TABLE     q   CREATE TABLE teachers (
    name text,
    lastname text,
    id bigint NOT NULL,
    field text,
    ma text
);
    DROP TABLE public.teachers;
       public         postgres    false    3            w          0    24577    lessons 
   TABLE DATA               [   COPY lessons (name, code, counter, teachercode, numberofstudents, term, field) FROM stdin;
    public       postgres    false    196   �       z          0    24606    selectedlessons 
   TABLE DATA               9   COPY selectedlessons (studentid, lessoncode) FROM stdin;
    public       postgres    false    199          x          0    24580    students 
   TABLE DATA               C   COPY students (name, lastname, id, field, counter, mt) FROM stdin;
    public       postgres    false    197   p       y          0    24588    teachers 
   TABLE DATA               :   COPY teachers (name, lastname, id, field, ma) FROM stdin;
    public       postgres    false    198   �       �           2606    24587    students PK 
   CONSTRAINT     D   ALTER TABLE ONLY students
    ADD CONSTRAINT "PK" PRIMARY KEY (id);
 7   ALTER TABLE ONLY public.students DROP CONSTRAINT "PK";
       public         postgres    false    197            �           2606    24600    lessons lessons_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY lessons
    ADD CONSTRAINT lessons_pkey PRIMARY KEY (code);
 >   ALTER TABLE ONLY public.lessons DROP CONSTRAINT lessons_pkey;
       public         postgres    false    196            �           2606    24595    teachers teachers_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY teachers
    ADD CONSTRAINT teachers_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.teachers DROP CONSTRAINT teachers_pkey;
       public         postgres    false    198            �           2606    24624    selectedlessons lessoncode    FK CONSTRAINT     r   ALTER TABLE ONLY selectedlessons
    ADD CONSTRAINT lessoncode FOREIGN KEY (lessoncode) REFERENCES lessons(code);
 D   ALTER TABLE ONLY public.selectedlessons DROP CONSTRAINT lessoncode;
       public       postgres    false    2038    196    199            �           2606    24609    selectedlessons studentid    FK CONSTRAINT     o   ALTER TABLE ONLY selectedlessons
    ADD CONSTRAINT studentid FOREIGN KEY (studentid) REFERENCES students(id);
 C   ALTER TABLE ONLY public.selectedlessons DROP CONSTRAINT studentid;
       public       postgres    false    197    2040    199            �           2606    24601    lessons treacher    FK CONSTRAINT     �   ALTER TABLE ONLY lessons
    ADD CONSTRAINT treacher FOREIGN KEY (teachercode) REFERENCES teachers(id) ON UPDATE CASCADE ON DELETE SET NULL;
 :   ALTER TABLE ONLY public.lessons DROP CONSTRAINT treacher;
       public       postgres    false    2042    198    196            w   h  x���Mn�0���9AE`~.�� BYp���a&R�h�=���3��S�H	�_�^�~:R���LGK���OI-�����>Ŗ���K��_�%�F'�Ԣx�:�X�z����Ksж)6wAkA+�mvB'���������NN\��.��V�{D��t�:,�*��7��7K�,h�>���V깎���ef'.�;�{.
�3�:i��~zH��]h^���)*���#ԉg�C����v�Z�CS\��D�~�P_���ppT_l����Ϟ�8U#φB��,e[�_C�/9�^��"rw+���Xr!邽�K�J2:�T�U� 3-rԬ�����c��S�$?��-�      z   I   x�M���0D�s��em���0�����8h�`�7fGf��:b�ڙ�rR`e����kg<��
�c�> <+ c      x     x��T�u�@�^�P>h� �%�؇��A� �P$��:f����e���I>8�w�>f�v�\V�������^V&�Y�N�3��5;>]V\цZ����Z:r��Ლ��4�d��/i�P�OJ_��fJ��?�Q�+|�@e*�КZ�H?�q��'���mmn��~ѣ��\.A���&"g=R��e`h����	.�qsM";i��Hk.G�<B�T��y�ư
"�_�v�Ҕ�����J[Iۤ�T��~�Krx�l���m�`�A$�͚�O���${~�U�~i�Q�."�?�j�Ź�𓜈�=�C/44W���'/=k�M����:HN���
�I^�+��#�qG�AA����E�C�W�!����hq���b������-i���t+A�%�n�ӹr������ jﷂ�����i�V��̛�#4��̘�B��@����o�Eg�%46�~w"�q��ʭZ꣕�R0��)�Ի���VsQ���NJ�f�nPl�����GP�gJ�L���>�`WD����0�L~4��      y   '  x��R�m�0|KSx�"M�f��G7HP�yx;E\�Q�"��A���NB���I���I�N�����M���`����\�Ҫ��ؠ���x�Hk��Z+�ip+�7x��/g���3�ǆ|��4 ;QODR*�&�H���♚��N��K�c/*p�C7�~���<)T���V>�C젵Am ���#���P�3�܍M2�?@��j�����_{6^��9ܱ/�s��Ys��������C���$8�9*��A"�&	?�@ �f'�QH߈�0���b��:sߩ|��� ,μ      