var {Client} = require('pg');


const client = new Client({
  host: 'localhost',
  database: 'iefinal',
  port: 5432,
  user: 'postgres',
  password: '0640618693'
})

client.connect()
  .then(() => {
    console.log("Database connected");
  })
  .catch(() => {
    console.log("Database error");
  });

// addStudent('r', 'b', '123456', 'bargh', 0, 2);
// addTeacher('a', 'b', 1234657890, 'computer', 'das');
// addLesson('asdca', 202, 2, 5123478625, 5, 'dd', 'bargh');
// getStudents();
// getStudentById(4213464512);
// getTeachers();
// getLessons();
// deleteStudent(4213464512);
// getTeacherByField("computer");
// getStudentByField("bargh");
// getLessonsByField("computer");
// selectLesson(123456, 101);
// updateStudent(9612762627, 'ali', 'nourollahi', 9612762627, 'computer', 1, 1);
// acceptedFinalLessons(123456);
// getFinalAcceptLesson(123456);
// deleteFinalAcceptLesson(123456);

function addStudent(name, lastName, id, field, counter, mt, callback) {
  console.log(mt);
  client.query("INSERT INTO students (name, lastName, id, field, counter, mt) VALUES ($1, $2, $3, $4, $5, $6)", [name, lastName, id, field, counter, mt])
    .then((res) => {
      callback(200);
    })
    .catch((err) => {
      callback(err.message);
    })
}


function addTeacher(name, lastName, id, field, ma, callback) {
  client.query("INSERT INTO teachers (name, lastName, id, field, ma) VALUES ($1, $2, $3, $4, $5)", [name, lastName, id, field, ma])
    .then((res) => {
      callback(200, "teacher added successfully");
    })
    .catch((err) => {
      callback(500, err.message);
    })
}


function addLesson(name, code, counter, teacherCode, numberOfStudents, te, field, callback) {
  client.query("INSERT INTO lessons (name, code, counter, teacherCode, numberOfStudents, term, field) VALUES ($1, $2, $3, $4, $5, $6, $7)", [name, code, counter, teacherCode, numberOfStudents, te, field])
    .then((res) => {
      callback(200, "lesson added successfully");
    })
    .catch((err) => {
      callback(500, err.message);
    })
}


function getStudents(callback) {
  client.query('select * from students')
    .then((res) => {
      callback(200, res.rows);
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function getStudentById(id, callback) {
  client.query('select * from students where id = $1', [id])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, res.rows);
      } else {
        callback(404, "student not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}


function getTeachers(callback) {
  client.query('select * from teachers')
    .then((res) => {
      callback(200, res.rows);
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function getTeacherById(id, callback) {
  client.query('select * from teachers where id = $1', [id])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, res.rows);
      } else {
        callback(404, "teacher not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function getLessons(callback) {
  client.query('select * from lessons')
    .then((res) => {
      callback(200, res.rows);
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function getLessonByCode(code, callback) {
  client.query('select * from lessons where code = $1', [code])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, res.rows);
      } else {
        callback(404, "lesson not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}


function deleteStudent(id, callback) {
  client.query('delete from students where id = $1', [id])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, "student deleted successfully");
      } else {
        callback(404, "student not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function deleteTeacher(id, callback) {
  client.query('delete from teachers where id = $1', [id])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, "teacher deleted successfully");
      } else {
        callback(404, "teacher not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}

function deleteLesson(code, callback) {
  client.query('delete from lessons where code = $1', [code])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, "lesson deleted successfully");
      } else {
        callback(404, "lesson not found");
      }
    })
    .catch((e) => {
      callback(500, e.message);
    })
}


function getTeacherByField(field, callback) {
  client.query("SELECT * FROM teachers where field = $1", [field])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, res.rows);
      } else {
        callback(404, "teacher not found");
      }
    })
    .catch((err) => {
      callback(500, err.message);
    })
}


function getStudentByField(field, callback) {
  client.query("SELECT * FROM students where field = $1", [field])
    .then((res) => {
      if (res.rowCount > 0) {

        callback(200, res.rows);
      } else {
        callback(404, "student not found");
      }
    })
    .catch((err) => {
      callback(500, err.message);
    })
}

function getLessonsByField(field, callback) {
  client.query("SELECT * from lessons where field = $1", [field])
    .then((res) => {
      callback(200, res.rows);
    })
    .catch((err) => {
      callback(500, err.message);
    })
}


function selectLesson(studentId, lessonCode, callback) {
  client.query("select field from students where id = $1", [studentId])
    .then((res) => {
      client.query("select * from lessons where field = $1 and code = $2", [res.rows[0].field, lessonCode])
        .then((res) => {
          if (res.rowCount > 0) {
            let counter = res.rows[0].counter;
            client.query("select * from selectedlessons where studentid = $1 and lessoncode = $2", [studentId, lessonCode])
              .then((res) => {
                if (res.rowCount === 0) {
                  client.query("insert into selectedlessons (studentid, lessoncode) values ($1, $2)", [studentId, lessonCode])
                    .then(() => {
                      incrementNumberOfStudents(lessonCode);
                      updateStudentCounter(studentId, counter);
                      callback(200, `lesson ${lessonCode} selected by student ${studentId}`);
                    })
                    .catch((err) => {
                      console.log(1);
                      callback(500, err.message);
                    })
                } else {
                  callback(400, `lesson ${lessonCode} already selected by student ${studentId}`);
                }
              })
              .catch((err) => {
                console.log(2);
                callback(500, err.message);
              })
          } else {
            callback(400, `student ${studentId} cannot select lesson ${lessonCode}`);
          }
        })
    })
    .catch((err) => {
      console.log(3);
      callback(500, err.message);
    })

}


function getSelectedLessons(studentId, callback) {
  client.query("select studentid , name , lessoncode , counter , term from selectedlessons INNER JOIN lessons ON lessons.code = selectedlessons.lessoncode where studentid = $1", [studentId])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200, res.rows);
      } else {
        callback(404, 'Not Found');
      }
    })
    .catch((err) => {
      callback(500, err.message);
    });
}


function deleteSelectedLessonsById(studentId, lessonCode, callback) {
  client.query("delete from selectedLessons where studentid = $1 and lessoncode = $2", [studentId, lessonCode])
    .then((res) => {
      if (res.rowCount > 0) {
        callback(200);
      } else {
        callback(404);
      }
    })
    .catch((err) => {
      callback(500);
    });
}

function updateStudentCounter(studentId, counter) {
  client.query(`select counter from students where id = ${studentId}`)
  .then((res) => {
    var c = res.rows[0].counter;
    console.log(c);
    console.log(counter);
    counter += c;
    client.query(`UPDATE students set counter = ${counter} where id = ${studentId}`)
    .then((res) => {
      console.log("student updated successfully");
    })
    .catch((err) => {
      console.log(err.message);
    });
  }).catch((err) => {
    console.log(err.message);
  })
  
}

function incrementNumberOfStudents(lessonCode) {
  client.query("select numberofstudents from lessons where code = $1", [lessonCode])
    .then((res) => {
      client.query("update lessons set numberofstudents = $1 where code = $2", [res.rows[0].numberofstudents + 1, lessonCode])
        .then(() => {
          console.log("number of student incremented successfully");
        })
        .catch((err) => {
          console.log(err.message);
        })
    })
    .catch((err) => {
      console.log(err.message);
    })
}

function updateStudent(studentId, name, lastname, field, counter, mt, callback) {
  client.query(`UPDATE students set name = '${name}', lastname = '${lastname}', field = '${field}', counter = ${counter}, mt = '${mt}' where id = ${studentId}`)
    .then((res) => {
      callback(200, "student updated successfully");
    })
    .catch((err) => {
      console.log(500, err.message);
    });
}

module.exports = {
  addStudent,
  updateStudent,
  getStudentById,
  getStudentByField,
  getStudents,
  deleteStudent,
  addTeacher,
  getTeacherById,
  getTeachers,
  getTeacherByField,
  deleteTeacher,
  addLesson,
  getLessonsByField,
  getLessonByCode,
  getLessons,
  deleteLesson,
  selectLesson,
  getSelectedLessons,
  deleteSelectedLessonsById
}
