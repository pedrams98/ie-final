const db = require("./db");

function setHeader(request, response, statusCode, contentType) {
  request.setEncoding("utf8");
  response.writeHead(statusCode, {"Content-Type": contentType});
}


//-------------------------------------
//students functions
//-------------------------------------
function addStudent(request, response, name, lastName, id, field, counter, mt) {
  db.addStudent(name, lastName, id, field, counter, mt, (status) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end("student added successfully");
    } else {
      setHeader(request, response, 400, 'text/plain');
      response.end();
    }
  });
}

function editStudent(request, response, studentId, name, lastname, field, counter, mt) {
  db.getStudentById(parseInt(studentId), (status, res) => {
    if (status === 200) {
      console.log(res[0]);
      if (name === '') {
        name = res[0].name;
      }
      if (lastname === '') {
        lastname = res[0].lastname;
      }
      if (field === '') {
        field = res[0].field;
      }
      if (counter === '') {
        counter = res[0].counter;
      }
      if (mt === '') {
        mt = res[0].mt;
      }

      db.updateStudent(studentId, name, lastname, field, counter, mt, (status, mes) => {
        setHeader(request, response, status, 'text/plain');
        response.end(mes);
      });
    } else {
      setHeader(request, response, status, 'text/plain');
      response.end(res);
    }
  });


}


function getStudentById(request, response, id) {
  db.getStudentById(parseInt(id), (status, mes) => {
    if (status === 200) {
      mes = JSON.stringify(mes);
      setHeader(request, response, 200, 'json/application');
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}

function getAllStudents(request, response) {
  db.getStudents((status, res) => {
    if (status === 200) {
      setHeader(request, response, 200, 'json/application');
      response.end(JSON.stringify(res));
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(res);
    }
  });
}

function getStudentByField(request, response, field) {

  db.getStudentByField(field, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'json/application');
      mes = JSON.stringify(mes);
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}

function deleteStudentById(request, response, id) {
  db.deleteStudent(id, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}


//-------------------------------------
//teachers functions
//-------------------------------------

function addTeacher(request, response, name, lastName, id, field, counter, me) {
  db.addTeacher(name, lastName, id, field, me, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  });
}


function getTeacherById(request, response, id) {
  db.getTeacherById(parseInt(id), (status, mes) => {
    if (status === 200) {
      mes = JSON.stringify(mes);
      setHeader(request, response, 200, 'json/application');
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}

function getAllTeachers(request, response) {
  db.getTeachers((status, res) => {
    if (status === 200) {
      setHeader(request, response, 200, 'json/application');
      response.end(JSON.stringify(res));
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(res);
    }
  })
}

function getTeacherByField(request, response, field) {

  db.getTeacherByField(field, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'json/application');
      mes = JSON.stringify(mes);
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}

function deleteTeacherById(request, response, id) {
  db.deleteTeacher(id, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}


//-------------------------------------
//lessons functions
//-------------------------------------

function addLesson(request, response, name, code, counter, teacherCode, numberOfStudents, te, field) {
  db.addLesson(name, code, counter, teacherCode, numberOfStudents, te, field, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  });
}


function getLessonsByField(request, response, field) {

  let m = 0;     
  db.getLessonsByField(field, (status, res) => {
    if (status === 200) {
      for (i = 0; i < res.length; i++) {
        db.getTeacherById(res[i].teachercode, (status, res1) => {
          if (status === 500) {
            setHeader(request, response, 500, 'json/application');
            response.end();
          } else {
            res[m].teacher = res1[0].name + " " +  res1[0].lastname;
          }
          m++;
          if (res.length === m) {

            setHeader(request, response, 200, 'json/application');
            response.end(JSON.stringify(res));
          }
        });
      }

    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(res);
    }
  })
}


function getLessonByCode(request, response, code) {

  db.getLessonByCode(code, (status, res) => {
    if (status === 200) {
        db.getTeacherById(res[0].teachercode, (status, res1) => {
          if (status === 500) {
            setHeader(request, response, 500, 'json/application');
            response.end();
          } else {
            res[0].teacher = res1[0].name + " " + res1[0].lastname;
          }
            setHeader(request, response, 200, 'json/application');
            response.end(JSON.stringify(res));
        });

    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(res);
    }
  })
}


function getAllLessons(request, response) {
  let m = 0;     
  db.getLessons((status, res) => {
    if (status === 200) {
      for (i = 0; i < res.length; i++) {
        db.getTeacherById(res[i].teachercode, (status, res1) => {
          if (status === 500) {
            setHeader(request, response, 500, 'json/application');
            response.end();
          } else {
            res[m].teacher = res1[0].name + res1[0].lastname;
          }
          m++;
          if (res.length === m) {

            setHeader(request, response, 200, 'json/application');
            response.end(JSON.stringify(res));
          }
        });
      }

    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(res);
    }
  })
}


function deleteLessonByCode(request, response, code) {
  db.deleteLesson(code, (status, mes) => {
    if (status === 200) {
      setHeader(request, response, 200, 'text/plain');
      response.end(mes);
    } else if (status === 404) {
      setHeader(request, response, 404, 'text/plain');
      response.end(mes);
    } else {
      setHeader(request, response, 500, 'text/plain');
      response.end(mes);
    }
  })
}



//-------------------------------------
//selectedlessons functions
//-------------------------------------


function selectLesson(request, response, studentId, lessonCode) {
  db.selectLesson(studentId, lessonCode, (status, res) => {
    setHeader(request, response, status, 'text/plain');
    response.end(res);
  })
}


function getSelectedLessonsById(request, response, studentId) {
  db.getSelectedLessons(studentId, (status, res) => {
    if (status === 200) {
      setHeader(request, response, status, 'json/application');
      response.end(JSON.stringify(res));
    } else {
      setHeader(request, response, status, 'text/plain');
      response.end(res);
    }
  })
}


function deleteSelectedLessonsById(request, response, studentId, lessonCode) {
  db.deleteSelectedLessonsById(studentId, lessonCode, (status) => {
    setHeader(request, response, status, 'text/plain');
    response.end();
  })
}


module.exports = {
  setHeader,
  addStudent,
  editStudent,
  getStudentById,
  getStudentByField,
  getAllStudents,
  deleteStudentById,
  addTeacher,
  getTeacherById,
  getAllTeachers,
  getTeacherByField,
  deleteTeacherById,
  addLesson,
  getLessonsByField,
  getLessonByCode,
  getAllLessons,
  deleteLessonByCode,
  selectLesson,
  getSelectedLessonsById,
  deleteSelectedLessonsById
}



