const functions = require('../controller/helpers/functions');
const url = require('url');

function route(handle, pathname, response, request) {
  console.log(pathname);
  if (handle[pathname] && handle[pathname][request.method]) {
    handle[pathname][request.method](request, response);
  } else {
    functions.setHeader(request, response, 404, "text/plain");
    response.write("404 Not found");
    response.end();
  }
}

var students = {
  'POST': (request, response) => {
    var postData = '';
    request.addListener("data", function (postDataChunk) {
      postData += postDataChunk;
    });
    request.addListener("end", function () {
      var dataJson = JSON.parse(postData);

      var name = dataJson.name;
      var lastName = dataJson.lastname;
      var id = dataJson.id;
      var field = dataJson.field;
      var counter = dataJson.counter;
      var mt = dataJson.mt;
      functions.addStudent(request, response, name, lastName, id, field, counter, mt);
    });
  },
  'PUT': (request, response) => {
    var postData = '';
    request.addListener("data", function (postDataChunk) {
      postData += postDataChunk;
    });
    request.addListener("end", function () {
      var dataJson = JSON.parse(postData);
      const queryObject = url.parse(request.url, true).query;
      const studentId = queryObject.id;
      let name = dataJson.name;
      let lastname = dataJson.lastname;
      let field = dataJson.field;
      let counter = dataJson.counter;
      let mt = dataJson.mt;
      if (!name) {
        name = "";
      }
      if (!lastname) {
        lastname = "";
      }
      if (!field) {
        field = "";
      }
      if (!counter) {
        counter = "";
      }
      if (!mt) {
        mt = "";
      }
      functions.editStudent(request, response, studentId, name, lastname, field, counter, mt);
    })
  },

  'GET': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const field = queryObject.field;
    const id = queryObject.id;
    if (field) {
      functions.getStudentByField(request, response, field);
    } else if (id) {
      functions.getStudentById(request, response, id);
    } else {
      functions.getAllStudents(request, response);
    }

  },

  'DELETE': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const id = queryObject.id;

    functions.deleteStudentById(request, response, id);

  }
}


var teachers = {
  'POST': (request, response) => {
    var postData = '';
    request.addListener("data", function (postDataChunk) {
      postData += postDataChunk;
    });
    request.addListener("end", function () {
      var dataJson = JSON.parse(postData);

      var name = dataJson.name;
      var lastName = dataJson.lastname;
      var id = dataJson.id;
      var field = dataJson.field;
      var me = dataJson.me;
      functions.addTeacher(request, response, name, lastName, id, field, me);
    });
  },

  'GET': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const field = queryObject.field;
    const id = queryObject.id;
    if (field) {
      functions.getTeacherByField(request, response, field);
    } else if (id) {
      functions.getTeacherById(request, response, id);
    } else {
      functions.getAllTeachers(request, response);
    }

  },

  'DELETE': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const id = queryObject.id;

    functions.deleteTeacherById(request, response, id);

  }
}


var lessons = {

  'POST': (request, response) => {
    var postData = '';
    request.addListener("data", function (postDataChunk) {
      postData += postDataChunk;
    });
    request.addListener("end", function () {
      var dataJson = JSON.parse(postData);
      var name = dataJson.name;
      var code = dataJson.code;
      var counter = dataJson.counter;
      var teacherCode = dataJson.teacherCode;
      var numberOfStudents = dataJson.numberOfStudents;
      var te = dataJson.te;
      var field = dataJson.field;
      functions.addLesson(request, response, name, code, counter, teacherCode, numberOfStudents, te, field);
    });
  },

  'GET': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const field = queryObject.field;
    const code = queryObject.code;
    if (field) {
      functions.getLessonsByField(request, response, field);
    } else if (code) {
      functions.getLessonByCode(request, response, code);
    } else {
      functions.getAllLessons(request, response);
    }

  },

  'DELETE': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const code = queryObject.code;
    functions.deleteLessonByCode(request, response, code);
  }

}


var selectedLessons = {
  'POST': (request, response) => {
    var postData = '';
    request.addListener("data", function (postDataChunk) {
      postData += postDataChunk;
    });
    request.addListener("end", function () {
      var dataJson = JSON.parse(postData);
      console.log(dataJson);
      var studentId = dataJson.studentid;
      var lessonCode = dataJson.lessoncode;
      functions.selectLesson(request, response, studentId, lessonCode);
    });
  },

  'GET': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const studentId = queryObject.id;
    functions.getSelectedLessonsById(request, response, studentId);
  },

  'DELETE': (request, response) => {
    const queryObject = url.parse(request.url, true).query;
    const studentId = queryObject.id;
    const lessonCode = queryObject.lessoncode;
    functions.deleteSelectedLessonsById(request, response, studentId, lessonCode);
  }
}



module.exports = {
  router: route,
  students,
  teachers,
  lessons,
  selectedLessons
}
