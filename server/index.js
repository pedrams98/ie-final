var server = require('./server');
var router = require('./route/router');

var handle = {};

handle['/'] = router.home;
handle['/myapi/students'] = router.students;
handle['/myapi/teachers'] = router.teachers;
handle['/myapi/lessons'] = router.lessons;
handle['/myapi/selectedlessons'] = router.selectedLessons;
server.start(router.router, handle);
